# Evaluating between Arango and Neo4j Graph Databases

## Introduction

- This repository aims to do a targeted POC on Arango and Neo4j as a Graph data store
- Gitlab wiki pages (as listed in 'Content' section) consists of the key notes for this POC

## Content

Gitlab wiki pages in this repository consists of:

- [Basic Concepts of Neo4j and Arango](https://gitlab.com/qingru97/graph-databases/-/wikis/Basic-Concepts-of-ArangoDB-and-Neo4j)
- [Data Modelling considerations](https://gitlab.com/qingru97/graph-databases/-/wikis/Data-Modelling)
- [Database related considerations](https://gitlab.com/qingru97/graph-databases/-/wikis/Database-related-Concepts)
- Evaluation of ArangoDB and Neo4j
    - [Technical examples for comparisons](https://gitlab.com/qingru97/graph-databases/-/wikis/Evaluation-of-ArangoDB-and-Neo4J-(Technical-Comparisons))
    - [Non-Technical comparisons](https://gitlab.com/qingru97/graph-databases/-/wikis/Evaluation-of-ArangoDB-and-Neo4J-(Non-Technical-Comparisons))

## Setup

Run non-persistent data single node Neo4j and arangoDB on docker:

- ArangoDB: username: `root`, password: `password`
- Neo4j: username: `neo4j`, password: `password`

```
docker-compose up
```

## Conclusion

- (placeholder for sharing slides)
